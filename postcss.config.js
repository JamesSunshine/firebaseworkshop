module.exports = {
    plugins: [
        require('tailwindcss'),
        require('autoprefixer')
    ],
    purge: {
        options: {
            safelist: [
                /data-theme$/,
            ],
        },
    },
}