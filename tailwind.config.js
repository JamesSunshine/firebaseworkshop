module.exports = {
  purge: {
    options: {
      safelist: [
        /data-theme$/,
      ],
    },
    enabled: true,
    content: [
      './src/**/*.vue',
      './public/**/*.html',
    ]
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('daisyui'),
  ],
  daisyui: {
    themes: [
      'light', // first one will be the default theme
      'cupcake',
      'bumblebee',
      'emerald',
      'corporate',
      'garden'
    ],
  },
}
