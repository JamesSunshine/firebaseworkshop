module.exports = {
    configureWebpack: {
        module: {
            rules: [{
                test: /\.mov$/,
                loader: 'file-loader'
            }]
        }
    }
}