import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

Vue.config.productionTip = false


const firebaseConfig = {


};

firebase.initializeApp(firebaseConfig);

Vue.use(VueRouter)
import "@/assets/styles/tailwind.css"

//Components
import Login from "@/pages/Login"
import Landing from "@/pages/Landing";
import Register from "@/pages/Register";

//Routes
const routes = [
  { path: '/', component: Login },
  { path: '/landing', component: Landing},
  { path: '/register', component: Register},

]

//Create Router
const router = new VueRouter({
  routes // short for `routes: routes`
})


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
