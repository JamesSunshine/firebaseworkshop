import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

export class FirebaseHelper {

    constructor() {
        this.db = firebase.firestore();
    }

    async firebaseRegister(email, password) {
        let message = firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((userCredential) => {
                let user = userCredential.user;
                localStorage.setItem("uid", user.uid);
            })
            .catch((error) => {
                let errorMessage = error.message;
                return errorMessage;
            });
        return message;
    }

    async firebaseLogin(email, password) {
        let message = firebase.auth().signInWithEmailAndPassword(email, password)
            .then((userCredential) => {
                // Signed in
                let user = userCredential.user;
                localStorage.setItem("uid", user.uid);
            })
            .catch((error) => {
                let errorMessage = error.message;
                return errorMessage;
            });
        return message;
    }

    async logout() {
        let message = firebase.auth().signOut()
            .then(function() {
                // Sign-out successful.
            })
            .catch(function(error) {
                return error
            });
        return message
    }

    isAuth() {
        let user = firebase.auth().currentUser;

        if (user){
            return user;
        } else {
            return false;
        }
    }

    async getUsername(uid) {
        let userData = this.db.collection("users").doc(uid).get();
        return userData.username;
    }

    async setUsername(uid, newUsername) {
        this.db.collection("users").doc(uid).update({
            username: newUsername
        })
        ;
    }

}
